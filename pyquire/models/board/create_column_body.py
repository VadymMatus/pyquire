from attr import attrs

from pyquire.models.column import Column

__all__ = ["CreateColumnBody"]


@attrs
class CreateColumnBody(Column):
    pass
