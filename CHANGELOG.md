# Changelog

**v1.0.4:**
1. Create facade
2. Add functools.wraps() decorator for each [api](pyquire/api.py) decorator
3. Integrate with [PyQuire](https://quire.io/apps/PyQuire)

**v1.0.1:**
1. Fix requirements

**v1.0.0:**
1. Just create